import s from './_structure.js';
// import pkg from '../package.json';

export default {
  html: {
    // entry: 'index.html',
  },
  css: {
    // Paths to bower components, if some vendor has a precompiled css we need
    vendors_compiled: [],

    // Paths to vendors that need to be compiled:
    // - copies in our own scss/vendors folder
    // - directly from bower_components/ folder
    vendors_to_compile: [
      `${s.src_css}/vendors/*.${s.src_css}`,
      `!${s.src_css}/vendors/_*.${s.src_css}`
    ]
  },
  js: {
    app: [
      // `${s.src_js}/module1.js`,
      // `${s.src_js}/module2.js`,
    ],
    libs: [
      // 'bower_components/jquery/dist/jquery.min.js',
      // 'bower_components/bootstrap/dist/js/bootstrap.js',
    ],
    // filesToCopy: [
    //   'bower_components/react/*.min.js',
    // ],
  },
  modernizr: {
    tests: [
      'touchevents',
    ],
    options: [
      'mq',
      'setClasses',
    ],
  },
};
