import { MakeDate } from './date.js';
// import { Chart } from 'chart.js';

export class ResultsHandler {
  constructor(data) {
    this.data = data;
    this.resultsContainer = document.getElementById('results');
    this.resultsContainer.innerHTML = '';
    this.pressureArray = [];
  }

  dailyForecastMarkup() {
    let dailyForecast = '';
    this.data.list.forEach((forecast) => {
      let date = new MakeDate(forecast.dt);
      this.pressureArray.push(forecast.pressure);
      dailyForecast += `
        <li class="day">
          <div class="info">
            <h4>${date.getDay()} ${date.getDate()} ${date.getMonth()}</h4>
            <ul class="list-unstyled">
              <li class="weather">${forecast.weather[0].description}</li>
              <li class="cloudiness">Cloudiness: ${forecast.clouds}%</li>
              <li class="humidity">Humidity: ${forecast.humidity}%</li>
              <li class="pressure">Pressure: ${forecast.pressure} hPa</li>
              <li class="winds">Wind: ${this.getWind(forecast.speed, forecast.deg)}</li>
            </ul>
          </div>
          <div class="temperature">
            <span class="temp-main">${Math.ceil(forecast.temp.day)}° C</span>
            <ul class="list-unstyled">
              <li><span class="temp-max">max: ${Math.ceil(forecast.temp.max)}° C</span></li>
              <li><span class="temp-min">min: ${Math.ceil(forecast.temp.min)}° C</span></li>
            </ul>
          </div>
        </li>
      `;
    })
    return dailyForecast
  }

  getWind(speed, deg) {
    let angle = deg - 180;
    let wind = `${speed} m/s
    <svg viewBox="0 0 512 512" style="transform: rotate(${angle}deg);">
      <path d="M128.4,189.3L233.4,89c5.8-6,13.7-9,22.4-9c8.7,0,16.5,3,22.4,9l105.4,100.3c12.5,11.9,12.5,31.3,0,43.2  c-12.5,11.9-32.7,11.9-45.2,0L288,184.4v217c0,16.9-14.3,30.6-32,30.6c-17.7,0-32-13.7-32-30.6v-217l-50.4,48.2 c-12.5,11.9-32.7,11.9-45.2,0C115.9,220.6,115.9,201.3,128.4,189.3z" />
    </svg>
    `;
    return wind;
  }


  drawChart() {
    let ctx = document.getElementById('forecast-chart');
    if (ctx === null) {
      let canvas = document.createElement('canvas');
      canvas.id = 'forecast-chart';
      canvas.classList.add('col-12');
      this.resultsContainer.parentNode.appendChild(canvas);
      ctx = document.getElementById('forecast-chart')
    }
    let chartData = {
      labels: [],
      datasets: [
        {
          label: 'Temperature',
          fill: false,
          lineTension: 0.3,
          backgroundColor: '#05c75f',
          borderColor: '#05c75f',
          borderWidth: 1,
          borderJoinStyle: 'miter',
          pointHoverBackgroundColor: '#05c75f',
          pointHoverBorderColor: '#05c75f',
          pointRadius: 1,
          pointHitRadius: 5,
          data: [],
          spanGaps: true
        },
        {
          label: 'min',
          fill: false,
          lineTension: 0.3,
          backgroundColor: 'rgba(0,0,0,0)',
          borderColor: '#5abaea',
          borderDash: [5],
          borderWidth: 1,
          borderJoinStyle: 'miter',
          pointHoverBackgroundColor: '#5abaea',
          pointHoverBorderColor: '#5abaea',
          pointRadius: 1,
          pointHitRadius: 5,
          data: [],
          spanGaps: true
        },
        {
          label: 'Max',
          fill: false,
          lineTension: 0.3,
          backgroundColor: 'rgba(0,0,0,0)',
          borderColor: '#d9534f',
          borderDash: [5],
          borderWidth: 1,
          borderJoinStyle: 'miter',
          pointHoverBackgroundColor: '#d9534f',
          pointHoverBorderColor: '#d9534f',
          pointRadius: 1,
          pointHitRadius: 5,
          data: [],
          spanGaps: true
        }
      ]
    };

    this.data.list.forEach((forecast, index) => {
      let date = new MakeDate(forecast.dt);
      chartData.labels.push(`${date.getDay()}, ${date.getDate()}`)
      chartData.datasets[0].data.push(Math.ceil(forecast.temp.day))
      chartData.datasets[1].data.push(Math.ceil(forecast.temp.min))
      chartData.datasets[2].data.push(Math.ceil(forecast.temp.max))
    })

    if (typeof(window.myLineChart) !== 'undefined') {
      window.myLineChart.destroy();
    }

    let myLineChart = new Chart(ctx, {
      type: 'line',
      data: chartData
    });

    window.myLineChart = myLineChart;
  }

  getAveragePressure() {
    let pressureAverageDiv = document.getElementById('pressure-average');
    if (pressureAverageDiv === null) {
      let div = document.createElement('div');
      div.id = 'pressure-average';
      div.classList.add('col-12');
      this.resultsContainer.parentNode.appendChild(div);
      pressureAverageDiv = document.getElementById('forecast-chart')
    }
    let average = (arr) => (arr.reduce( ( p, c ) => p + c, 0 )) / arr.length;
    pressureAverageDiv.innerHTML = `Average weekly pressure: ${average(this.pressureArray).toFixed(2)} hPa`;
  }

  appendResults() {
    this.resultsContainer.innerHTML = this.dailyForecastMarkup();
    this.drawChart();
    this.getAveragePressure();
  }

}