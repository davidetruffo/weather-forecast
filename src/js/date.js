export class MakeDate {

  constructor(timestamp) {
    this.date = new Date(timestamp * 1000);
    this.days = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday'
    ];

    this.months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];
  }

  getDay() {
    return this.days[this.date.getDay()];
  }

  getMonth() {
    return this.months[this.date.getMonth()];
  }

  getDate() {
    return this.date.getDate();
  }
}
