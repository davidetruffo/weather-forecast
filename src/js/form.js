import { makeRequest } from './ajax.js';
import { ResultsHandler } from './results-handler.js';

export class Form {

  constructor(form) {
    this.form = form;
    this.addEventListeners();
  }

  addEventListeners() {
    const option = this.form.querySelector('[name="city"]');
    this.form.addEventListener('submit', (e) => this.submitForm(e));
    option.addEventListener('change', (e) => this.submitForm(e));
  }

  submitForm(e) {
    const uri = 'http://api.openweathermap.org/data/2.5/forecast/daily';
    const api_key = '1e099a8aa4b7f5aa104cc961280a468c';
    let city = e.target.selectedOptions[0].value;
    let country = e.target.selectedOptions[0].dataset.countryCode;
    let url = `${uri}?q=${city},${country}&appid=${api_key}&mode=json&units=metric&cnt=7`;

    makeRequest('get', url)
      .then((response) => {
        let resultsHandler = new ResultsHandler(JSON.parse(response));
        resultsHandler.appendResults();
      })
      .catch((error) => {
        console.log(error);
      })
  }
}