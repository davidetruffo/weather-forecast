import { Form } from './form.js';

import '../scss/vendors/bootstrap.scss';
import '../scss/styles.scss';


(() => new Form(document.getElementById('weather-form')))();
