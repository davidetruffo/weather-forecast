'use strict'

var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');
var path = require('path');

var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
// var CopyWebpackPlugin = require('copy-webpack-plugin');



module.exports = {
  context: __dirname,
  devtool: 'inline-source-map',
  entry: {
    main: path.resolve(__dirname, 'src', 'js', 'index.js'),
    vendors: [
      'chart.js'
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.min.js',
    publicPath: '/',
  },
  devServer: {
    contentBase: './dist'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: path.join(__dirname, 'node_modules'),
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env', 'es2015',]
          }
        }
      },
      {
        test: /\.scss$/,
        exclude: path.join(__dirname, 'node_modules'),
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader']
        })
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'images/',
        }
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendors',
      chunks: ['vendors'],
      minChunk: Infinity
    }),
    new HtmlWebpackPlugin({
      hash: true,
      showErrors: true,
      template: './src/html/index.ejs'
    }),
    new ExtractTextPlugin("styles.css"),
  ]
};
